#Pig Latin
# Rule 1: If a word begins with a vowel sound, add an "ay" sound to
# the end of the word.
# Rule 2: If a word begins with a consonant sound, move it to the end
# of the word, and then add an "ay" sound to the end of the word.
# rule 3: qu is consider as a consonant
#input: " a quick hello there"
#ouput:"aay ickquay ellohay erethay"
def translate(sentence)
  sentence.split.map {|word| word = translate_word(word)}.join(" ")
end

def translate_word(word)
   vowels = "aeiou"
   i = 0
   while i < word.length
     if vowels.include?(word[i]) && i == 0
       return "#{word}ay"
     elsif vowels.include?(word[i]) && word[i-1..i] != "qu"
       return "#{word[i..-1]}#{word[0..i-1]}ay"
     end
     i += 1
   end
end

#need to refactor
