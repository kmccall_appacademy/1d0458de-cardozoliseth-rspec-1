#return a word
def echo(word)
  word
end

#return sentence all upcase
def shout(sentence)
  sentence.upcase
end

#repeats the word n times
def repeat(word, repeats=2)
  repeats_word = []
  repeats_word << word while repeats_word.length < repeats
  repeats_word.join(" ")
end

#return several letters acording to number
def start_of_word(word, number)
  word.slice(0,number)
end

#return the first word off a sentence
def first_word(sentence)
  sentence.split[0]
end

#return the a capitalize sentence excluding litle words
def titleize(sentence)
  words = sentence.split
  little_words = ["in","on", "the", "over", "and", "a", "an"]
  (0...words.length).each do |idx|
    if !little_words.include?(words[idx]) || idx == 0
     words[idx] = words[idx].capitalize
    end
  end
  words.join(" ")
end
